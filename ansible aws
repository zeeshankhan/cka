---
- name: Provision EC2 Instance
  hosts: localhost
  connection: local
  gather_facts: no
  vars:
    region: us-east-1
    instance_type: t2.micro
    ami: ami-0ac80df6eff0e70b5  # Ubuntu Server 18.04 LTS
    disk_size: 20
    volume_type: gp2
    security_group_name: ansible-security-group
    key_name: ansible-keypair
    ssh_port: 22
    http_port: 80
    https_port: 443

  tasks:
    - name: Create security group
      ec2_group:
        name: "{{ security_group_name }}"
        description: "Allow SSH, HTTP, and HTTPS"
        region: "{{ region }}"
        rules:
          - proto: tcp
            from_port: "{{ ssh_port }}"
            to_port: "{{ ssh_port }}"
            cidr_ip: 0.0.0.0/0
          - proto: tcp
            from_port: "{{ http_port }}"
            to_port: "{{ http_port }}"
            cidr_ip: 0.0.0.0/0
          - proto: tcp
            from_port: "{{ https_port }}"
            to_port: "{{ https_port }}"
            cidr_ip: 0.0.0.0/0
        rules_egress:
          - proto: all
            cidr_ip: 0.0.0.0/0

    - name: Launch EC2 instance
      ec2:
        key_name: "{{ key_name }}"
        instance_type: "{{ instance_type }}"
        image: "{{ ami }}"
        region: "{{ region }}"
        group: "{{ security_group_name }}"
        count: 1
        wait: yes
        instance_tags:
          Name: ansible-ec2-instance
        volumes:
          - device_name: /dev/sda1
            volume_size: "{{ disk_size }}"
            volume_type: "{{ volume_type }}"
        assign_public_ip: yes
      register: ec2_instance

    - name: Add new SSH key pair
      ec2_key:
        name: "{{ key_name }}"
        region: "{{ region }}"
      when: ec2_instance.changed




    - name: Add new instance to host group
      add_host:
        hostname: "{{ item.public_ip }}"
        ansible_ssh_private_key_file: ~/.ssh/{{ key_name }}.pem
        ansible_user: ubuntu
        ansible_python_interpreter: /usr/bin/python3
      with_items: "{{ ec2_instance.instances }}"

      export AWS_ACCESS_KEY_ID='your_access_key_id'
export AWS_SECRET_ACCESS_KEY='your_secret_access_key'

